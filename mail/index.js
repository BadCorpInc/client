const request = require('request')

module.exports = function(config) {
    return {
        sendTemplate(options) {
            return new Promise((resolve, reject) => {
                if (options.template == 'recover') options.template = '5872'
                else if (options.template == 'confirm_mail') options.template = '5843'
                options.subject = options.subject || ''
                this.sendMail(options, resolve, reject)
            })
        },
        sendMail(options, resolve, reject) {
            request.post({
                    url: 'https://api.elasticemail.com/v2/email/send',
                    form: Object.assign(options, {
                        from: 'noreply@badcorpinc.com',
                        fromName: 'BadCorp Inc.',
                        isTransactional: true,
                        apikey: config.elasticemail_key
                    })
                },
                function (error, response, body) {
                if(!error) body = JSON.parse(body)
                if (error || !body.success) {
                    reject(error || body.error)
                } else {
                    resolve(body)
                }
            });
        }
    }
}