const mongoose = require('mongoose')
const util = require('../util')
 

const MailRandKeySchema = new mongoose.Schema({
    email: String,
    randomKey: String,
    createdAt: { type: Date, expires: 60 * 60 * 24 } // expires is in seconds
})

MailRandKeySchema.pre('save', function(next) { 
    this.createdAt = new Date()
    this.randomKey = util.randomString()
    next()
})

module.exports = mongoose.model('MailRandKey', MailRandKeySchema)