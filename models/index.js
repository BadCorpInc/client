const User = require('../models/user')
const MailRandKey = require('../models/mailRandKey')

module.exports = {
    User,
    MailRandKey
}