const bCrypt = require('bcrypt-nodejs')
const crypto = require('crypto')

module.exports = {
    createPasswordHash(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null)
    },
    comparePasswordHash(user, password) {
        return bCrypt.compareSync(password, user.password)
    },
    randomString(size = 32) {
        return crypto.randomBytes(size).toString('hex')
    }
}