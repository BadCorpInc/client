const LocalStrategy   = require('passport-local').Strategy
const dbModels = require('../models')
const util = require('../util')

module.exports = function(passport) {
	passport.use('login', new LocalStrategy({
            usernameField: 'email',
            passReqToCallback : true
        },
        function(req, email, password, done) {
            // check in mongo if a user with username exists or not
            dbModels.User.findOne({ 'email' :  {$regex: new RegExp(email, "i")} },
                function(err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Email does not exist, log the error and redirect back
                    if (!user) {
                        return done(null, false, req.flash('message', 'User Not found.'));                 
                    }
                    // User exists but wrong password, log the error
                    if (!util.comparePasswordHash(user, password)) {
                        return done(null, false, req.flash('message', 'Invalid Password')) // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success
                    return done(null, user);
                }
            );

        })
    )
}