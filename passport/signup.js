const LocalStrategy = require('passport-local').Strategy
const dbModels = require('../models')
const util = require('../util')

module.exports = function (passport) {
    passport.use('signup', new LocalStrategy({
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
        function () {
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser, ...arguments)
        })
    )
}

function findOrCreateUser(req, username, password, done) {
    checkUserRequirements(req, username, req.body.email).then(function () {
        var newUser = new dbModels.User({
            username,
            email: req.body.email,
            password: util.createPasswordHash(password)
        })

        newUser.save(function (err) {
            if (err) {
                console.log('Error in Saving user: ' + err)
                throw err
            }
            done(null, newUser)
        })
    }).catch(function () {
        done(...arguments)
    })
}

function checkUserRequirements(req, username, email) {
    return new Promise(function (resolve, reject) {
        checkUserRequirement(dbModels.MailRandKey, { randomKey: req.params.randomKey }).then(function (mailRandKey) {
            if (!mailRandKey) return reject(null, false, req.flash('message', 'Error while searching for randomKey'))

            checkUserRequirement(dbModels.User, { username: { $regex: new RegExp(username, "i") } }).then(function (data) {
                if (data) return reject(null, false, req.flash('message', 'Username Already Exists'))

                checkUserRequirement(dbModels.User, { email: { $regex: new RegExp(email, "i") } }).then(function (data) {
                    if (data) return reject(null, false, req.flash('message', 'Email Already Exists'))

                    if (mailRandKey && mailRandKey.email === email) {
                        mailRandKey.remove()
                        resolve(...arguments)
                    } else {
                        reject(...arguments, req.flash('message', 'Emails don\'t match'))
                    }

                }).catch(function () { reject(...arguments) })
            }).catch(function () { reject(...arguments) })
        }).catch(function () { reject(...arguments) })
    })
}


function checkUserRequirement(model, findObj) {
    return new Promise(function (resolve, reject) {
        model.findOne(findObj, function (err, data) {
            if (err) {
                return reject(err)
            }
            resolve(data)
        })
    })
}