const crypto = require('crypto')
const config = require('../config')
const discourse_sso = require('discourse-sso')
const sso = new discourse_sso(config.discourse_sso_secret)

module.exports = function(router, passport) {
	/* GET discourse page. */
	router.get('/discourse', function(req, res) {
		if (!req.isAuthenticated()) return res.redirect('/?continue=' + encodeURI('https://community.badcorpinc.com/login'))
		if (sso.validate(req.query.sso, req.query.sig)) {
			const newPayload = {
				nonce: sso.getNonce(req.query.sso),
				email: req.user.email,
				username: req.user.username,
				external_id: req.user._id.toString()
			}
			res.redirect('https://community.badcorpinc.com/session/sso_login?' + sso.buildLoginString(newPayload))
		} else {
			req.flash('message', 'SIG invalid.')
			res.redirect('/')
		}
	})
}