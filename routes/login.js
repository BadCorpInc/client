module.exports = function(router, passport) {
	/* GET login page. */
	router.get('/', function(req, res) {
		if (req.query.continue) req.session.continue = req.query.continue
		if (req.isAuthenticated()) return res.redirect('/home')
    	// Display the Login page with any flash message, if any
		res.render('index', { message: req.flash('message') })
	});

	/* Handle Login POST */
	router.post('/login', passport.authenticate('login', {
		successRedirect: '/home',
		failureRedirect: '/',
		failureFlash : true  
	}));
}