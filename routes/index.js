const express = require('express')
const router = express.Router()
const route_recover = require('./recover')
const route_login = require('./login')
const route_discourse = require('./discourse')
const route_registration = require('./registration')

module.exports = function(passport, mail){
	route_discourse(router, passport)
	route_login(router, passport)
	route_registration(router, passport, mail)
	route_recover(router, mail)

	/* GET Home Page */
	router.get('/home', function(req, res) {
		if (!req.isAuthenticated()) return res.redirect('/')
		const authkey = 'test'

		// Remove the first subdomain and add dot at the start (Ex: sso.badcorpinc.com -> .badcorpinc.com)
		var domain = req.headers.host.split('.')
		domain.shift()
		domain = '.' + domain.join('.')

		res.cookie('authkey', authkey, {
			domain,
			maxAge: 1000 * 60,
			httpOnly: true
		})

		const redirectUrl = req.session.continue || false
		if (req.session.continue) req.session.continue = false
		res.render('home', {
			user: req.user,
			redirectUrl
		})
	})

	/* Handle Logout */
	router.get('/signout', function(req, res) {
		req.logout()
		res.redirect('/')
	})

	return router
}
