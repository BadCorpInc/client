const dbModels = require('../models')
const util = require('../util')

module.exports = function(router, mail) {
	/* GET Forgot password Page */
	router.get('/recover', function(req, res){
		if (req.isAuthenticated()) return res.redirect('/')
		res.render('recover', {message: req.flash('message')})
    })

	/* GET Forgot password Page */
	router.get('/recover/:randomKey', function(req, res){
		if (req.isAuthenticated()) return res.redirect('/')
		res.render('recover', {randomKey: req.params.randomKey, message: req.flash('message')})
    })

	/* POST Forgout password Page */
	router.post('/recover', function(req, res){
        if (req.isAuthenticated()) return res.redirect('/')
        if (req.body.email) sendEmail(req, res)
        else if (req.body.randomKey) changePassword(req, res)
        else res.render('recover', {message: req.flash('message')})
    })

    function changePassword(req, res) {
        dbModels.MailRandKey.findOne({randomKey: req.body.randomKey}, function(err, mailRandKey) {
            if (err){
                res.render('recover', {message: 'Error while searching for randomKey'})
                throw err
            }
            if (mailRandKey) {
                mailRandKey.remove()
                const newPass = util.createPasswordHash(req.body.password)
                dbModels.User.findOneAndUpdate({email: mailRandKey.email}, {password: newPass}, function(err) {
                    if (err) {
                        res.render('recover', {message: 'Error while saving random key'})
                        throw err
                    }
                    req.flash('message', 'Password changed. Please login with the new password')
                    res.redirect('/')
                })
            } else {
                res.render('recover', {message: 'This link has expired. (Random Key not found)'})
            }
        })
    }

    function sendEmail(req, res) {
        dbModels.User.findOne({email: { $regex: new RegExp(req.body.email, "i") }}, function(err, user) {
            if (err){
                res.render('recover', {message: 'Error while searching for mail'})
                throw err
            }
            if (user) {
                var mailRandKey = new dbModels.MailRandKey()
                mailRandKey.email = req.body.email
                mailRandKey.save(function(err) {
                    if (err) {
                        res.render('recover', {message: 'Error while saving random key'})
                        throw err
                    }
                    mail.sendTemplate({
                        template: 'recover',
                        to: req.body.email,
						merge_buttonUrl: `https://${req.headers.host}/recover/${mailRandKey.randomKey}`
                    }).then(() => {
						res.render('recover', {message: 'Email sent. Check your inbox or spam folders'})
					}).catch(err => {
						res.render('recover', {message: 'Error sending email. ' + err})
					})
                })
            } else {
                res.render('recover', {message: 'Email not found'})
            }
        })
    }
}