const dbModels = require('../models')
const util = require('../util')

module.exports = function(router, passport, mail) {
	/* GET Registration Page */
	router.get('/signup', function(req, res){
		if (req.isAuthenticated()) return res.redirect('/')
		res.render('signup', {message: req.flash('message')})
	})

	/* POST Registration Page */
	router.post('/signup', function(req, res) {
		dbModels.User.findOne({email: req.body.email}, (err, user) => {
			if (err) {
				res.render('signup', {message: 'Error while confirming email existence'})
				throw err
			}
			if (user) {
				res.render('signup', {message: 'Email already exists'})
			} else {
                const newEmailConfirmLink = new dbModels.MailRandKey()
                newEmailConfirmLink.email = req.body.email
                newEmailConfirmLink.save(function(err) {
                    if (err) {
                        res.render('signup', {message: 'Error while saving random key'})
                        throw err
                    }
                    mail.sendTemplate({
                        template: 'confirm_mail',
						to: req.body.email,
						merge_buttonUrl: `https://${req.headers.host}/signup/${newEmailConfirmLink.randomKey}`
                    }).then(() => {
						res.render('signup', {message: 'Confirmation email sent. Check your inbox or spam folders'})
					}).catch(err => {
						res.render('signup', {message: 'Error sending email. ' + err})
					})
                })
			}
		})
	})

	/* GET Confirm Mail Page */
	router.get('/signup/:randomKey', function(req, res) {
		if (req.isAuthenticated()) return res.redirect('/')
		dbModels.MailRandKey.findOne({randomKey: req.params.randomKey}, function(err, mailrand) {
			if (err || !mailrand) {
				res.render('signup', {message: 'Error while searching for random key'})
			} else {
				res.render('signup_final', {message: req.flash('message'), randomKey: req.params.randomKey, email: mailrand.email})
			}
		})
	})

	/* POST Confirm Mail Page */
	router.post('/signup/:randomKey', function(req, res, next) {
		passport.authenticate('signup', {
			failureFlash : true  
		}, function(err, user, info) {
		  if (err) { return next(err) }
		  if (!user) { return res.redirect('/signup/' + req.params.randomKey) }
		  req.logIn(user, function(err) {
			if (err) { return next(err) }
			return res.redirect('/home')
		  })
		})(req, res, next)
	})
}